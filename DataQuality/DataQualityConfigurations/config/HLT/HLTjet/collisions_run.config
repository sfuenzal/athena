######################################################################
# $Id: collisions_run.config Mon Jun 27 15:01:21 2022 ayana $
# $Id: collisions_run.config Thur Jan 16 15:01:21 2022 xingguo $  
######################################################################

#######################
# HLTjet
#######################
#The following block is kept because if signature reference is different than the centrally provided ones, the reference can be still changed
#reference HLTJetRef {
#  location = /eos/atlas/atlascerngroupdisk/data-dqm/references/Collisions/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/
#  file = data15_13TeV.00267638.physics_EnhancedBias.merge.HIST.r6857_p1831.root
#  path = run_267638
#  name = same_name
#}
algorithm HLTjetSimpleSummary {
  libname = libdqm_summaries.so
  name = SimpleSummary
}
compositeAlgorithm HLTjet_Histogram_Not_Empty&GatherData {
  subalgs = GatherData,Histogram_Not_Empty
  libnames = libdqm_algorithms.so
}
algorithm HLTjet_Histogram_Not_Empty&GatherData {
  name = HLTjet_Histogram_Not_Empty&GatherData
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;CentrallyManagedReferences_Trigger
}
algorithm HLTjet_Histogram_Not_Empty_with_Ref&GatherData {
  name = HLTjet_Histogram_Not_Empty&GatherData
  #reference = HLTJetRef
  #reference = HLT_local_reference
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;CentrallyManagedReferences_Trigger 
}
compositeAlgorithm HLTjet_GatherData&Chi2Test {
  subalgs = GatherData,Chi2Test_Prob
  libnames = libdqm_algorithms.so
}
algorithm HLTjet_Chi2NDF {
  name = HLTjet_GatherData&Chi2Test_Chi2_per_NDF
  thresholds = HLTjet_Chi2_Thresh
  #reference = HLTJetRef
  #reference = HLT_local_reference
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;CentrallyManagedReferences_Trigger
  MinStat = 1000
}
thresholds HLTjet_Chi2_Thresh {
  limits Chi2_per_NDFt {
    warning = 5
    error = 8
  }
}
algorithm HLTjet_KolmogorovTest_MaxDist {
  libname = libdqm_algorithms.so 
  name = KolmogorovTest_MaxDist
  thresholds = HLTjet_KolmogorovThresh
  MinStat = 100
  #reference = HLTJetRef
  #reference = HLT_local_reference
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;CentrallyManagedReferences_Trigger
}
thresholds HLTjet_KolmogorovThresh {
  limits MaxDist {
    warning = 0.1
    error = 0.5
  }
}
thresholds HLTjetEta_BinsDiff_Threshold {
  limits MaxDeviation {
    warning = 3.0
    error = 6.0
  }
}
algorithm HLTjetEtaPhiAve_BinsDiff {
  libname = libdqm_algorithms.so
  name = GatherData&BinsDiffStrips
  PublishBins = 1
  MaxPublish = 10
  SigmaThresh = 0.
  xmin = -3.15
  xmax = 3.15
  MinStat = 15000
  TestConsistencyWithErrors = 0
  thresholds = HLTjetEta_BinsDiff_Threshold
  #reference = HLTJetRef
  #reference = HLT_local_reference
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}

#######################
# Output
#######################
output top_level {
  algorithm = SimpleSummary
  output HLT {
    output TRJET {
      output Shifter {
        output L1 {
          output jFexSRJetRoI {   
            output ${l1jFexSRShifterChain}     {  
            }     
          }
          output gFexSRJetRoI {   
            output ${l1gFexSRShifterChain}     {  
            }     
          }
          output gFexLRJetRoI {   
            output ${l1gFexLRShifterChain}     {  
            }     
          }
        }
        output Online {  
          output ${shifterJetCollection} {
    	      output ${hltShifterChain} {
	          }
          }
        }       
      }
      output Expert {
        output L1 { 
          output ${l1MonGroup} {
      	    output L1_${l1Chain} {
	          }
            output MatchedRecoJets {
            }
            output MatchedTrigJets {
            }    
          }
        }  
        output Online {
          output HLT_AntiKt${radius}${constits}_${calibEtcExp} {
    	      output ${hltChain} {
            }	  	  
          }
        } 
        output Offline {
          output AntiKt4EMTopoJets {
	          output MatchedPFlowJets {
            }           
            output LooseBadFailedJets {
            }
          }
          output AntiKt4EMPFlowJets {
            output LooseBadFailedJets {
            }
          }
        } 
      }
    }
  }
}


#######################
# Histogram Assessments
#######################
dir HLT {
  dir JetMon {
    dir Online {
      algorithm     = HLTjet_Histogram_Not_Empty_with_Ref&GatherData
      dir HLT_AntiKt(?P<radius>\d*)(?P<constits>\w*)Jets_(?P<calibEtcExp>\w*) {
        regex         = 1
        display       = StatBox   
        dir HLT_(?P<hltChain>\d*(noalg|j)\w*) {
          hist (?!trigEff)\w*@expert {
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt${radius}${constits}_${calibEtcExp}/${hltChain}
          }
        }
        dir NoTriggerSelection {
          hist eta_phi@expert {
            output =  HLT/TRJET/Expert/Online/HLT_AntiKt${radius}${constits}_${calibEtcExp}
            algorithm = HLTjetEtaPhiAve_BinsDiff
            display = DRAW=COLZ
          }           
          hist (et_eta|pt_m|phi_e)@expert {
            output =  HLT/TRJET/Expert/Online/HLT_AntiKt${radius}${constits}_${calibEtcExp}
            algorithm = HLTjet_Histogram_Not_Empty&GatherData
            display = DRAW=COLZ
          }
        }
      }
      dir (?P<shifterJetCollection>HLT_AntiKt(10EMPFlowCSSKSoftDropBeta100Zcut10Jets_jes_ftf|4EMPFlowJets_subresjesgscIS_ftf|4EMTopoJets_subjesIS)) {
        regex         = 1
        display       = StatBox
        dir NoTriggerSelection {
          hist eta_phi@shifter {
            output =  HLT/TRJET/Shifter/Online/${shifterJetCollection}
            algorithm = HLTjetEtaPhiAve_BinsDiff
            display = DRAW=COLZ
          }       
          hist (et_eta|pt_m|phi_e)@shifter {
            output =  HLT/TRJET/Shifter/Online/${shifterJetCollection}
            algorithm = HLTjet_Histogram_Not_Empty&GatherData
            display = DRAW=COLZ
          }  
        }
        dir (?P<hltShifterChain>HLT_(j\d+_L1|j\d+_pf_ftf_preselj\d+_L1|j\d+_a10sd_cssk_pf_jes_ftf_preselj\d+_L1|3j\d+_pf_ftf_L1jJ\d+|[5,6]j\d+c_pf_ftf_presel[5,6]c\d+_L|j0_HT\w*_L1|2j\d+\w*_a10sd_cssk_pf_jes_ftf_presel2j\d+_L1)\w*) {
          hist (?!trigEff)\w*@shifter {
            output        = HLT/TRJET/Shifter/Online/${shifterJetCollection}/${hltShifterChain}
          }
        }
      }
    }
    dir Offline {
      description   = Use reco jet response and kinematics to debug issues in trigger jets.
      algorithm     = HLTjet_Histogram_Not_Empty_with_Ref&GatherData
      dir AntiKt4EMTopoJets {
        dir standardHistos {
          hist .*@expert {
            regex         = 1
            output        = HLT/TRJET/Expert/Offline/AntiKt4EMTopoJets
            display       = StatBox
          }
          dir MatchedJets_AntiKt4EMPFlowJets {
            # Need to name this differently (expert vs expert2)
            # otherwise there's a name clash with histograms from a different directory
            hist .*@expert2 {
              regex         = 1
              output        = HLT/TRJET/Expert/Offline/AntiKt4EMTopoJets/MatchedPFlowJets
              display       = StatBox
            }
          }    
        }
        dir LooseBadFailedJets {
          hist .*@expert {
            regex         = 1
            algorithm     = HLTjet_Histogram_Not_Empty_with_Ref&GatherData
            output        = HLT/TRJET/Expert/Offline/AntiKt4EMTopoJets/LooseBadFailedJets
            display       = StatBox
          }
        }
      }
      dir AntiKt4EMPFlowJets {
        dir standardHistos {
          hist .*@expert {
            regex         = 1
            output        = HLT/TRJET/Expert/Offline/AntiKt4EMPFlowJets
            display       = StatBox
          }
        }
        dir LooseBadFailedJets {
          hist .*@expert {
            regex         = 1
            algorithm     = HLTjet_Histogram_Not_Empty_with_Ref&GatherData
            output        = HLT/TRJET/Expert/Offline/AntiKt4EMPFlowJets/LooseBadFailedJets
            display       = StatBox
          }
        }
      }
    }  
    dir L1 {
      algorithm     = HLTjet_Histogram_Not_Empty&GatherData
      dir L1_(?P<l1MonGroup>.*) {
        regex           = 1
        dir NoTriggerSelection {
          dir MatchedJets_A.* {
            hist .*@expert {
              description   = Look for changes in ${l1MonGroup} response with respect to offline jets
              output        = HLT/TRJET/Expert/L1/${l1MonGroup}/MatchedRecoJets
              display       = StatBox
            }
          }
          dir MatchedJets_H.* {
            hist .*@expert {
              description   = Look for changes in ${l1MonGroup} response with respect to HLT
              output        = HLT/TRJET/Expert/L1/${l1MonGroup}/MatchedTrigJets
              display       = StatBox
            }
          }
          hist \w*@expert {
            description   = Look for changes in ${l1MonGroup} kinematics
            output        = HLT/TRJET/Expert/L1/${l1MonGroup}
            display       = StatBox
          }
        }
        dir L1_(?P<l1Chain>.*) {
          hist .*@expert {
            description   = Check threshold and look for changes in RoIs
            output        = HLT/TRJET/Expert/L1/${l1MonGroup}/L1_${l1Chain}
            display       = StatBox
          }
        }
      }
      dir L1_jFexSRJetRoI {
        dir (?P<l1jFexSRShifterChain>L1_jJ\d+) {
          regex = 1
          hist .*@shifter {
            description   = Check threshold and look for changes in RoIs
            output        = HLT/TRJET/Shifter/L1/jFexSRJetRoI/${l1jFexSRShifterChain}
            display       = StatBox
          }
        }
        dir NoTriggerSelection {
          hist \w*@shifter {
            regex         = 1
            description   = Look for changes in jFexSRJetRoI kinematics
            output        = HLT/TRJET/Shifter/L1/jFexSRJetRoI
            display       = StatBox
          }
        }
      }
      dir L1_gFexSRJetRoI {
        dir (?P<l1gFexSRShifterChain>L1_gJ\d+p0ETA25) {
          regex = 1
          hist .*@shifter {
            description   = Check threshold and look for changes in RoIs
            output        = HLT/TRJET/Shifter/L1/gFexSRJetRoI/${l1gFexSRShifterChain}
            display       = StatBox
          }
        }
        dir NoTriggerSelection {
          hist \w*@shifter {
            regex         = 1
            description   = Look for changes in gFexSRJetRoI kinematics
            output        = HLT/TRJET/Shifter/L1/gFexSRJetRoI
            display       = StatBox
          }
        }
      }
      dir L1_gFexLRJetRoI {
        dir (?P<l1gFexLRShifterChain>L1_gLJ\d+p0ETA25) {
          regex = 1
          hist .*@shifter {
            description   = Check threshold and look for changes in RoIs
            output        = HLT/TRJET/Shifter/L1/gFexLRJetRoI/${l1gFexLRShifterChain}
            display       = StatBox
          }
        }
        dir NoTriggerSelection {
          hist \w*@shifter {
            regex         = 1
            description   = Look for changes in gFexLRJetRoI kinematics
            output        = HLT/TRJET/Shifter/L1/gFexLRJetRoI
            display       = StatBox
          }
        }
      }
    }
  }
}

