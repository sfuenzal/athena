# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

# flake8: noqa

from .powheg_control import PowhegControl
